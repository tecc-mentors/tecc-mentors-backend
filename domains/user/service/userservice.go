package service

import (
	"teccmentors/domains/user/model"
	"teccmentors/domains/user/repository"
)

func Find() (*[]model.User, error) {
	return repository.Find()
}

func Create(user *model.User) error {
	return repository.Create(user)
}
