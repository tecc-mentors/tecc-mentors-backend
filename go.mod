module teccmentors

go 1.16

require (
	github.com/go-playground/validator/v10 v10.5.0 // indirect
	github.com/gofiber/fiber/v2 v2.8.0
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/json-iterator/go v1.1.11 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/lib/pq v1.10.1 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/ugorji/go v1.2.5 // indirect
	golang.org/x/crypto v0.0.0-20210421170649-83a5a9bb288b // indirect
	golang.org/x/sys v0.0.0-20210426230700-d19ff857e887 // indirect
	golang.org/x/text v0.3.6 // indirect
	gopkg.in/dealancer/validate.v2 v2.1.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	xorm.io/xorm v1.0.7
)
